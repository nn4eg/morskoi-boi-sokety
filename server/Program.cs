﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace server
{
    class Program
    {
        enum SystemCommands
        {
            MSG_START_GAME,
            MSG_STOP_GAME,
            MSG_FIELD_GAMEOVER,
            MSG_FIELD_SHOW,
            MSG_FIELD_STEP,
            MSG_ERROR
        }

        const string Ip = "127.0.0.1";
        const int Port = 23556;

        static void SendMessageToClient(Socket client, string message)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(message);
            client.Send(bytes);
        }

        static string CreateMessage(SystemCommands command, string parametres, string parametres2)
        {
            return (int)command + "|" + parametres + parametres2;
        }

        static string TakeMessageFromClient(Socket client)
        {
            string message = String.Empty;
            do
            {
                byte[] bytes = new byte[1024];
                int count = client.Receive(bytes);
                message += Encoding.Unicode.GetString(bytes, 0, count);
            }
            while (client.Available != 0);
            return message;
        }

        static int GetICoord(string message)
        {
            return int.Parse(message.Split('|')[0]) - 1;
        }

        static int GetJCoord(string message)
        {
            return int.Parse(message.Split('|')[1]) - 1;
        }

        static void Logs(string text)
        {
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(text);
            Console.WriteLine("=====================");
            Console.WriteLine();
        }

        #region LOGIKA
        const char ShipA = 'A';
        const char ShipB = 'B';
        const char DeadShip = 'X';
        const char Miss = 'O';
        const char EMPTY = '.';
        enum GameResult { WinA, WinB, Continue }

        static void ResetField(char[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = EMPTY;
                }
            }
        }

        static void ResetField2(char[,] field2)
        {
            for (int i = 0; i < field2.GetLength(0); i++)
            {
                for (int j = 0; j < field2.GetLength(1); j++)
                {
                    field2[i, j] = EMPTY;
                }
            }
        }

        static string FieldToString(char[,] field)
        {
            string output = String.Empty;
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    output += field[i, j];
                }
                output += "\n";
            }
            output += "\n";
            output += "\n";

            return output;
        }

        static string FieldToString2(char[,] field2)
        {
            string output = String.Empty;
            for (int i = 0; i < field2.GetLength(0); i++)
            {
                for (int j = 0; j < field2.GetLength(1); j++)
                {
                    if (field2[i, j] == ShipB)
                    {
                        output += EMPTY;
                    }
                    else
                    {
                        output += field2[i, j];
                    }
                }
                output += "\n";
            }
            output += "\n";
            output += "\n";

            return output;
        }

        static string FieldToString3(char[,] field2)
        {
            string output = String.Empty;
            for (int i = 0; i < field2.GetLength(0); i++)
            {
                for (int j = 0; j < field2.GetLength(1); j++)
                {
                    output += field2[i, j];
                }
                output += "\n";
            }
            output += "\n";
            output += "\n";

            return output;
        }

        static string FieldToString4(char[,] field)
        {
            string output = String.Empty;
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    if (field[i, j] == ShipA)
                    {
                        output += EMPTY;
                    }
                    else
                    {
                        output += field[i, j];
                    }
                }
                output += "\n";
            }
            output += "\n";
            output += "\n";

            return output;
        }

        static void FillShip1Field(char[,] field)
        {
            Random rnd = new Random();
            int i, j;
            for (int c = 0; c < 10; c++)
            {
                do
                {
                    i = rnd.Next(1, 10);
                    j = rnd.Next(1, 10);
                }
                while (field[i, j] != EMPTY);
                field[i, j] = ShipA;
            }
            System.Threading.Thread.Sleep(20);
        }

        static void FillShip2Field(char[,] field2)
        {
            Random rnd1 = new Random();
            int i, j;
            for (int c = 0; c < 10; c++)
            {
                do
                {
                    i = rnd1.Next(1, 10);
                    j = rnd1.Next(1, 10);
                }
                while (field2[i, j] != EMPTY);
                field2[i, j] = ShipB;
            }
        }


        static bool MakeStep(char[,] field, int i, int j, char sign)
        {
            if (i < 0 || i > 10 || j < 0 || j > 10)
            {
                return false;
            }
            if (field[i, j] == EMPTY)
            {
                field[i, j] = sign;
                return true;
            }
            if(field[i,j]==ShipA)
            {
                field[i, j] = DeadShip;
                return true;
            }
            else
            {
                return false;
            }
        }

        static bool MakeStep2(char[,] field2, int i, int j, char sign)
        {
            if (i < 0 || i > 10 || j < 0 || j > 10)
            {
                return false;
            }
            if (field2[i, j] == EMPTY)
            {
                field2[i, j] = sign;
                return true;
            }
            if (field2[i, j] == ShipB)
            {
                field2[i, j] = DeadShip;
                return true;
            }
            else
            {
                return false;
            }
        }

        static GameResult WhoWin(char[,] field, char[,] field2)
        {
            int countDeadShips = 0;
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    if (field[i, j] == DeadShip)
                    {
                        countDeadShips++;
                    }
                }
            }
            int countDeadShips2 = 0;
            for (int i = 0; i < field2.GetLength(0); i++)
            {
                for (int j = 0; j < field2.GetLength(1); j++)
                {
                    if (field2[i, j] == DeadShip)
                    {
                        countDeadShips2++;
                    }
                }
            }
            if (countDeadShips == 10)
            {
                return GameResult.WinB;
            }
            if (countDeadShips2 == 10)
            {
                return GameResult.WinA;
            }
            return GameResult.Continue;
        }

        static bool StopGame(Socket ClientShipA, Socket ClientShipB, char[,] field, char[,] field2)
        {
            GameResult gameResult = WhoWin(field, field2);
            if (gameResult != GameResult.Continue)
            {
                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_FIELD_GAMEOVER, FieldToString(field), null));
                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_FIELD_GAMEOVER, FieldToString(field2), null));
            }
            if (gameResult == GameResult.WinA)
            {
                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_STOP_GAME, "Вы победили", null));
                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_STOP_GAME, "Вы проиграли", null));
                return true;
            }
            if (gameResult == GameResult.WinB)
            {
                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_STOP_GAME, "Вы проиграли", null));
                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_STOP_GAME, "Вы победили", null));
                return true;
            }
            if (gameResult == GameResult.Continue)
            {
                return false;
            }
            return false;
        }
        #endregion

        static void Main(string[] args)
        {

            char[,] field = new char[10, 10];
            char[,] field2 = new char[10, 10];

            ResetField(field);
            ResetField(field2);
            FillShip1Field(field);
            FillShip2Field(field2);

            int gamestep = 0;
            bool playgame = true;
            Socket server = null;

            try
            {
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Bind(new IPEndPoint(IPAddress.Any, Port));
                server.Listen(1);
            }
            catch
            {
                Console.WriteLine("Невозможно создать сервер - нажмите Enter для выхода из программы");
                Console.ReadKey();
                Environment.Exit(0);
            }
            Socket ClientShipA = null;
            Socket ClientShipB = null;


            try
            {
                ClientShipA = server.Accept();
                Logs("Подключился первый игрок " + ClientShipA.RemoteEndPoint);

                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_START_GAME, "Вы успешно подключены... Ожидаем подключение второго игрока.", null));

                ClientShipB = server.Accept();
                Logs("Подключился второй игрок " + ClientShipB.RemoteEndPoint);

                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_START_GAME, "Вы успешно подключены, сейчас игра начнется", null));
                System.Threading.Thread.Sleep(2000);

                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_FIELD_STEP, FieldToString(field), FieldToString2(field2)));

                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_FIELD_SHOW, FieldToString3(field2), FieldToString4(field)));

                while (playgame)
                {
                    gamestep++;

                    if (gamestep % 2 != 0)
                    {
                        bool rightStep;

                        do
                        {
                            string message = TakeMessageFromClient(ClientShipA);
                            int i = GetICoord(message);
                            int j = GetJCoord(message);

                            if (rightStep = MakeStep2(field2, i, j, Miss))            
                            {
                                if (StopGame(ClientShipA, ClientShipB, field, field2))
                                {
                                    playgame = false;
                                }
                                else
                                {
                                    SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_FIELD_SHOW, FieldToString(field), FieldToString2(field2)));

                                    SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_FIELD_STEP, FieldToString3(field2), FieldToString4(field)));
                                }
                            }
                            else
                            {
                                SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_ERROR, "Данная ячейка уже занята", null));
                            }
                        }
                        while (!rightStep);
                        Logs("Шаг игры: " + gamestep + "\n" + FieldToString2(field2));
                    }
                    else
                    {
                        bool rightStep;

                        do
                        {
                            string message = TakeMessageFromClient(ClientShipB);
                            int i = GetICoord(message);
                            int j = GetJCoord(message);

                            if (rightStep = MakeStep(field, i, j, Miss))
                            {
                                if (StopGame(ClientShipA, ClientShipB, field, field2))
                                {
                                    playgame = false;
                                }
                                else
                                {
                                    SendMessageToClient(ClientShipA, CreateMessage(SystemCommands.MSG_FIELD_STEP, FieldToString(field), FieldToString2(field2)));

                                    SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_FIELD_SHOW, FieldToString3(field2), FieldToString4(field)));
                                }
                            }
                            else
                            {
                                SendMessageToClient(ClientShipB, CreateMessage(SystemCommands.MSG_ERROR, "Данная ячейка уже занята", null));
                            }
                        }
                        while (!rightStep);
                        Logs("Шаг игры: " + gamestep + "\n" + FieldToString4(field));
                    }
                    
                }
            }

            catch
            {
                ClientShipA.Close();
                ClientShipB.Close();
                server.Close();

                Console.WriteLine("Соединение разорвано - нажмите Enter для выхода из программы");

                Console.ReadKey();

                Environment.Exit(0);
            }
            finally
            {
                ClientShipA.Shutdown(SocketShutdown.Both);
                ClientShipA.Close();

                ClientShipB.Shutdown(SocketShutdown.Both);
                ClientShipB.Close();

                server.Close();

                Console.WriteLine("Соединение разорвано - нажмите Enter для выхода из программы");

                Console.ReadKey();
            }

        }
    }
}
