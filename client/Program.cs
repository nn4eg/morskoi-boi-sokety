﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace client
{
    class Program
    {
        enum SystemCommands
        {
            MSG_START_GAME,
            MSG_STOP_GAME,
            MSG_FIELD_GAMEOVER,
            MSG_FIELD_SHOW,
            MSG_FIELD_STEP,
            MSG_ERROR
        }

        const string Ip = "127.0.0.1";
        const int Port = 23556;
        static void SendMessageToServer(Socket server, string message)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(message);
            server.Send(bytes);
        }

        static string CreateMessage(int i, int j)
        {
            return i + "|" + j;
        }

        static string TakeMessageFromServer(Socket server)
        {
            string message = String.Empty;
            do
            {
                byte[] bytes = new byte[1024];
                int count = server.Receive(bytes);
                message += Encoding.Unicode.GetString(bytes, 0, count);
            }
            while (server.Available != 0);
            return message;
        }

        static SystemCommands GetSystemCommand(string message)
        {
            return (SystemCommands)int.Parse(message.Split('|')[0]);
        }

        static string GetParametes(string message)
        {
            return message.Split('|')[1];
        }

        static int InputInt(string text)
        {
            int val;
            bool isParse;

            do
            {
                Console.Write(text);
                isParse = int.TryParse(Console.ReadLine(), out val);
            }
            while (!isParse);

            return val;
        }

        static void Main(string[] args)
        {
            int i, j;
            bool playGame = true;
            Socket server = null;

            try
            {
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                server.Connect(IPAddress.Parse(Ip), Port);
            }
            catch
            {
                Console.WriteLine("Сервер недоступен - нажмите Enter для выхода из программы");
                Console.ReadKey();
                Environment.Exit(0);
            }
            try
            {
                while (playGame)
                {
                    string message = TakeMessageFromServer(server);
                    SystemCommands command = GetSystemCommand(message);
                    string parameters = GetParametes(message);

                    switch (command)
                    {
                        case SystemCommands.MSG_START_GAME:
                            Console.WriteLine(parameters);
                            break;
                        case SystemCommands.MSG_STOP_GAME:
                            Console.WriteLine(parameters);
                            playGame = false;
                            break;
                        case SystemCommands.MSG_FIELD_GAMEOVER:
                            Console.Clear();
                            Console.WriteLine(parameters);
                            break;
                        case SystemCommands.MSG_FIELD_SHOW:
                            Console.Clear();
                            Console.WriteLine(parameters);
                            Console.WriteLine("Ожидаем хода другого игрока");
                            break;
                        case SystemCommands.MSG_FIELD_STEP:
                            Console.Clear();
                            Console.WriteLine(parameters);
                            do
                            {
                                i = InputInt("Введите строку: ");
                                j = InputInt("Введите столбец: ");                              
                            }
                            while (i < 1 || i > 10 || j < 0 || j > 10);
                            SendMessageToServer(server, CreateMessage(i, j));
                            break;
                        case SystemCommands.MSG_ERROR:
                            Console.WriteLine(parameters);
                            do
                            {
                                i = InputInt("Введите строку: ");
                                j = InputInt("Введите столбец: ");
                            }
                            while (i < 1 || i > 10 || j < 0 || j > 10);
                            SendMessageToServer(server, CreateMessage(i, j));

                            break;
                    }
                }
            }
            catch
            {
                server.Shutdown(SocketShutdown.Both);
                server.Close();
                Console.WriteLine("Соединение разорвано - нажмите Enter для выхода из программы");
                Console.ReadKey();
                Environment.Exit(0);

            }
            finally
            {
                server.Shutdown(SocketShutdown.Both);
                server.Close();
                Console.WriteLine("Соединение разорвано - нажмите Enter для выхода из программы");
                Console.ReadKey();
            }
        }
    }
}
